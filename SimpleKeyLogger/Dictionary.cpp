#include "Dictionary.h"


Dictionary::Dictionary(string keyboardLanguage) {
    this->keyboardCode = selectLayout(keyboardLanguage);
    this->dictionary = getDictionary();
    computeMinMaxKeyCodes();
}


Dictionary::~Dictionary(void) {
}


string Dictionary::translate(int key) {
    return dictionary[key];
}


int Dictionary::getMinKeyCode() {
    return this->minKeyCode;
}


int Dictionary::getMaxKeyCode() {
    return this->maxKeyCode;
}


void Dictionary::computeMinMaxKeyCodes() {
    static set<int> keys = getKeys();
    int minimum = INT_MAX;
    int maximum = INT_MIN;
    for (set<int>::iterator it = keys.begin(); it != keys.end(); ++it) {
        minimum = min(minimum, *it);
        maximum = max(maximum, *it);
    }
    this->minKeyCode = minimum;
    this->maxKeyCode = maximum;
}


int Dictionary::selectLayout(string keyboardLanguage) {
    if (keyboardLanguage.compare("Brazilian ABNT") == 0)
        return Brazilian_ABNT;
    else
        return US_English;
}


set<int> Dictionary::getKeys() {
    static map<int, string> dictionary = getDictionary();
    static set<int> keys;
    for (map<int,string>::iterator it = dictionary.begin(); it != dictionary.end(); ++it)
        keys.insert(it->first);
    return keys;
}


map<int, string> Dictionary::getDictionary() {
    static map<int, string> dict;
    dict[0xC1] = translate0xC1();
    dict[0xC2] = "[Abnt C2]";
    dict[0x6B] = "+";
    dict[0xF6] = "[Attn]";
    dict[0x08] = "[Backspace]";
    dict[0x03] = "[Break]";
    dict[0x0C] = "[Clear]";
    dict[0xF7] = "[Cr Sel]";
    dict[0x6E] = ".";
    dict[0x6F] = "/";
    dict[0xF9] = "[Er Eof]";
    dict[0x1B] = "[Esc]";
    dict[0x2B] = "[Execute]";
    dict[0xF8] = "[Ex Sel]";
    dict[0xE6] = "[IcoClr]";
    dict[0xE3] = "[IcoHlp]";
    dict[0x30] = "0";
    dict[0x31] = "1";
    dict[0x32] = "2";
    dict[0x33] = "3";
    dict[0x34] = "4";
    dict[0x35] = "5";
    dict[0x36] = "6";
    dict[0x37] = "7";
    dict[0x38] = "8";
    dict[0x39] = "9";
    dict[0x41] = "a";
    dict[0x42] = "b";
    dict[0x43] = "c";
    dict[0x44] = "d";
    dict[0x45] = "e";
    dict[0x46] = "f";
    dict[0x47] = "g";
    dict[0x48] = "h";
    dict[0x49] = "i";
    dict[0x4A] = "j";
    dict[0x4B] = "k";
    dict[0x4C] = "l";
    dict[0x4D] = "m";
    dict[0x4E] = "n";
    dict[0x4F] = "o";
    dict[0x50] = "p";
    dict[0x51] = "q";
    dict[0x52] = "r";
    dict[0x53] = "s";
    dict[0x54] = "t";
    dict[0x55] = "u";
    dict[0x56] = "v";
    dict[0x57] = "w";
    dict[0x58] = "x";
    dict[0x59] = "y";
    dict[0x5A] = "z";
    dict[0x6A] = "*";
    dict[0xFC] = "[NoName]";
    dict[0x60] = "0";
    dict[0x61] = "1";
    dict[0x62] = "2";
    dict[0x63] = "3";
    dict[0x64] = "4";
    dict[0x65] = "5";
    dict[0x66] = "6";
    dict[0x67] = "7";
    dict[0x68] = "8";
    dict[0x69] = "9";
    dict[0xBA] = translate0xBA();
    dict[0xE2] = "[< or >]";
    dict[0xBF] = translate0xBF();
    dict[0xC0] = "[~ or `]";
    dict[0xDB] = translate0xDB();
    dict[0xDC] = translate0xDC();
    dict[0xDD] = translate0xDD();
    dict[0xDE] = translate0xDE();
    dict[0xDF] = "[� or !]";
    dict[0xF0] = "[Oem Attn]";
    dict[0xF3] = "[Auto]";
    dict[0xE1] = "[Ax]";
    dict[0xF5] = "[Back Tab]";
    dict[0xFE] = "[OemClr]";
    dict[0xBC] = translate0xBC();
    dict[0xF2] = "[Copy]";
    dict[0xEF] = "[Cu Sel]";
    dict[0xF4] = "[Enlw]";
    dict[0xF1] = "[Finish]";
    dict[0x95] = "[Loya]";
    dict[0x93] = "[Mashu]";
    dict[0x96] = "[Roya]";
    dict[0x94] = "Touroku";
    dict[0xEA] = "Jump";
    dict[0xBD] = "[_ or -]";
    dict[0xEB] = "[OemPa1]";
    dict[0xEC] = "[OemPa2]";
    dict[0xED] = "[OemPa3]";
    dict[0xBE] = "[> or .]";
    dict[0xBB] = "[+ or =]";
    dict[0xE9] = "[Reset]";
    dict[0xEE] = "[WsCtrl]";
    dict[0xFD] = "[Pa1]";
    dict[0xE7] = "[Packet]";
    dict[0xFA] = "[Play]";
    dict[0xE5] = "[Process]";
    dict[0x0D] = "[Enter]";
    dict[0x29] = "[Select]";
    dict[0x6C] = "[Separator]";
    dict[0x20] = " ";
    dict[0x6D] = "-";
    dict[0x09] = "[Tab]";
    dict[0xFB] = "[Zoom]";
    dict[0xFF] = "[NoMapped]";
    dict[0x1E] = "[Accept]";
    dict[0x5D] = "[Context Menu]";
    dict[0xA6] = "[Browser Back]";
    dict[0xAB] = "[Browser Favorites]";
    dict[0xA7] = "[Browser Forward]";
    dict[0xAC] = "[Browser Home]";
    dict[0xA8] = "[Browser Refresh]";
    dict[0xAA] = "[Browser Search]";
    dict[0xA9] = "[Browser Stop]";
    dict[0x14] = "[CAPSLOCK]";
    dict[0x1C] = "[Convert]";
    dict[0x2E] = "[Delete]";
    dict[0x28] = "[Arrow Down]";
    dict[0x23] = "[End]";
    dict[0x70] = "[F1]";
    dict[0x79] = "[F10]";
    dict[0x7A] = "[F11]";
    dict[0x7B] = "[F12]";
    dict[0x7C] = "[F13]";
    dict[0x7D] = "[F14]";
    dict[0x7E] = "[F15]";
    dict[0x7F] = "[F16]";
    dict[0x80] = "[F17]";
    dict[0x81] = "[F18]";
    dict[0x82] = "[F19]";
    dict[0x71] = "[F2]";
    dict[0x83] = "[F20]";
    dict[0x84] = "[F21]";
    dict[0x85] = "[F22]";
    dict[0x86] = "[F23]";
    dict[0x87] = "[F24]";
    dict[0x72] = "[F3]";
    dict[0x73] = "[F4]";
    dict[0x74] = "[F5]";
    dict[0x75] = "[F6]";
    dict[0x76] = "[F7]";
    dict[0x77] = "[F8]";
    dict[0x78] = "[F9]";
    dict[0x18] = "[Final]";
    dict[0x2F] = "[Help]";
    dict[0x24] = "[Home]";
    dict[0xE4] = "[Ico00]";
    dict[0x2D] = "[Insert]";
    dict[0x17] = "Junja";
    dict[0x15] = "[Kana]";
    dict[0x19] = "[Kanji]";
    dict[0xB6] = "[App1]";
    dict[0xB7] = "[App2]";
    dict[0xB4] = "[Mail]";
    dict[0xB5] = "[Media]";
    dict[0x01] = "[Mouse Left Button]";
    dict[0x02] = "[Mouse Right Button]";
    dict[0xA2] = "[Left Ctrl]";
    dict[0x25] = "[Arrow Left]";
    dict[0xA4] = "[Left Alt]";
    dict[0xA0] = "[Left Shift]";
    dict[0x5B] = "[Left Win]";
    dict[0x04] = "[Mouse Middle Button]";
    dict[0xB0] = "[Next Track]";
    dict[0xB3] = "[Play/Pause]";
    dict[0xB1] = "[Previous Track]";
    dict[0xB2] = "[Stop]";
    dict[0x1F] = "[Mode Change]";
    dict[0x22] = "[Page Down]";
    dict[0x1D] = "[Non Convert]";
    dict[0x90] = "[Num Lock]";
    dict[0x92] = "[Jisho]";
    dict[0x13] = "[Pause]";
    dict[0x2A] = "[Print]";
    dict[0x21] = "[Page Up]";
    dict[0xA3] = "[Right Ctrl]";
    dict[0x27] = "[Arrow Right]";
    dict[0xA5] = "[Right Alt]";
    dict[0xA1] = "[Right Shift]";
    dict[0x5C] = "[Right Win]";
    dict[0x91] = "[Scrol Lock]";
    dict[0x5F] = "[Sleep]";
    dict[0x2C] = "[Print Screen]";
    dict[0x26] = "[Arrow Up]";
    dict[0xAE] = "[Volume Down]";
    dict[0xAD] = "[Volume Mute]";
    dict[0xAF] = "[Volume Up]";
    dict[0x05] = "[X Button 1]";
    dict[0x06] = "[X Button 2]";
    return dict;
}


inline string Dictionary::translate0xBC() {
    switch(keyboardCode) {
    case Brazilian_ABNT:
        return "[| or \\]";
    default:
        return "[< or ,]";
    }
}


inline string Dictionary::translate0xBA() {
    switch(keyboardCode) {
    case Brazilian_ABNT:
        return "�";
    default:
        return "[: or ;]";
    }
}


inline string Dictionary::translate0xDC() {
    switch(keyboardCode) {
    case Brazilian_ABNT:
        return "[} or ]]";
    default:
        return "[| or \\]";
    }
}


inline string Dictionary::translate0xDE() {
    switch(keyboardCode) {
    case Brazilian_ABNT:
        return "[~ or ^]";
    default:
        return "[\" or ']";
    }
}


inline string Dictionary::translate0xDD() {
    switch(keyboardCode) {
    case Brazilian_ABNT:
        return "[{ or []";
    default:
        return "[} or ]]";
    }
}


inline string Dictionary::translate0xDB() {
    switch(keyboardCode) {
    case Brazilian_ABNT:
        return "[` or ']";
    default:
        return "[{ or ]]";
    }
}


inline string Dictionary::translate0xC1() {
    switch(keyboardCode) {
    case Brazilian_ABNT:
        return "[? or /]";
    default:
        return "[Abnt C1]";
    }
}


inline string Dictionary::translate0xBF() {
    switch(keyboardCode) {
    case Brazilian_ABNT:
        return "[; or :]";
    default:
        return "[? or /]";
    }
}