#include "HttpConnection.h"
#include "Constants.h"
#include <string>
#include <Shlwapi.h>
#pragma comment( lib,"Wininet.lib")
#include <WinInet.h>


HttpConnection::HttpConnection(void) {
    fill_n(lpBufferResponse, 512, 0);
    fill_n(postOptionalBuffer, 1024, 0);
}


HttpConnection::~HttpConnection(void) {
}


int HttpConnection::openSession() {
    hInternet = InternetOpen(USER_AGENT, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
    if(!hInternet)
        return 0;
    return 1;
}


int HttpConnection::connect() {
    hConnect = InternetConnect(hInternet, REMOTE_SERVER, INTERNET_DEFAULT_HTTP_PORT, NULL, NULL, INTERNET_SERVICE_HTTP, 0, 1);
    if(!hConnect)
        return 0;
    return 1;
}


int HttpConnection::prepareRequest() {
    LPCTSTR dwAccept[2] = {"*/*", NULL};
    hFileRequest = HttpOpenRequest(hConnect, "POST", REMOTE_SERVER_PROCESS_URI, NULL, NULL, dwAccept, 0 ,1);
    if(!hFileRequest)
        return 0;
    return 1;
}


int HttpConnection::sendRequest() {
    string lpszHeaders = "Content-Type: application/x-www-form-urlencoded";
    unsigned long headersLength = lpszHeaders.length();
    unsigned long postOptionalBufferLength = strlen(postOptionalBuffer);
    if(!HttpSendRequest(hFileRequest, (LPCTSTR) lpszHeaders.c_str(), headersLength, postOptionalBuffer, postOptionalBufferLength))
        return 0;
    return 1;
}


void HttpConnection::closeHandles() {
    InternetCloseHandle(hFileRequest);
    InternetCloseHandle(hConnect);
    InternetCloseHandle(hInternet);
}


int HttpConnection::send(string content, string machine) {
    DWORD dwNumberOfBytesToRead = 512;
    unsigned long lpdwNumberOfBytesRead = 0;
    sprintf_s(postOptionalBuffer, 1024, REMOTE_SERVER_VARIABLES, (LPSTR) machine.c_str(), (LPSTR) content.c_str());
    if (!openSession() || !connect() || !prepareRequest() || !sendRequest())
        return 0;
    while(InternetReadFile(hFileRequest, &lpBufferResponse, dwNumberOfBytesToRead, &lpdwNumberOfBytesRead)) {
        if(!lpdwNumberOfBytesRead)
            break;
        lpBufferResponse[lpdwNumberOfBytesRead] = '\0';
        remoteContent += string(lpBufferResponse);
    }
    closeHandles();
    return 1;
}