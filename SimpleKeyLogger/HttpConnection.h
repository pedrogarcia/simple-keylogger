#ifndef HTTPCONNECTION_H
#define HTTPCONNECTION_H

#include <string>
#include <Shlwapi.h>
#pragma comment( lib,"Wininet.lib")
#include <WinInet.h>

using namespace std;

class HttpConnection
{
public:
    HttpConnection(void);
    ~HttpConnection(void);
    int openSession();
    int connect();
    int prepareRequest();
    int sendRequest();
    void closeHandles();
    int send(string, string);

private:
    HINTERNET hFileRequest;
    HINTERNET hConnect;
    HINTERNET hInternet;
    string remoteContent;
    char lpBufferResponse[512];
    char postOptionalBuffer[1024];
};

#endif