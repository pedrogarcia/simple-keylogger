#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <string>
#include <map>
#include <set>

using namespace std;

class Dictionary
{
public:
    Dictionary(string keyboardLanguage="US English");
    ~Dictionary(void);
    string translate(int);
    int getMinKeyCode(void);
    int getMaxKeyCode(void);
private:
    int keyboardCode;
    map<int, string> dictionary;
    int minKeyCode;
    int maxKeyCode;

    enum Layouts {
        US_English = 0x00,
        Brazilian_ABNT = 0x01
    };

    void computeMinMaxKeyCodes();
    int selectLayout(string);
    set<int> getKeys();
    map<int, string> getDictionary();
    string translate0xBC();
    string translate0xBA();
    string translate0xDC();
    string translate0xDE();
    string translate0xDD();
    string translate0xDB();
    string translate0xC1();
    string translate0xBF();
};

#endif