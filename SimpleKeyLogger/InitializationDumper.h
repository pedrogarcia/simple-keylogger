#ifndef INITIALIZATIONDUMPER_H
#define INITIALIZATIONDUMPER_H

#include <string>

using namespace std;

class InitializationDumper
{
public:
    InitializationDumper(void);
    ~InitializationDumper(void);
    void dump(void);
private:
    string getStartupFolderPath(void);
    string getThisExecutablePath(void);
    string extractFilenameFromPath(string);
    void copy(string, string);
};

#endif